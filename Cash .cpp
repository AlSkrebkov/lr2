#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>
using namespace std;

class Cache
{
	unsigned char *p;
	int Size;
	//int Nanosec; // ���� �� �����
	int SizeB;
	static int N;//����� ��������
public:
	// ����������� �����������
	Cache(const Cache & C)
	{
		Size = C.Size;
		SizeB = C.SizeB;
		p = new unsigned char[SizeB]; // �������� ������ ��� ����� ������
		// �������� ������
		for (int i = 0; i < SizeB; i++) p[i] = C.p[i];

	}

	// ����������� �����������
	Cache(Cache && C)
	{
		Size = C.Size;
		SizeB = C.SizeB;
		// ������ �� ��������, � ���������� �� ������������� �������
		p = C.p;
		C.p = nullptr; // ������ ������ �� �����, ������, ��� ���������� �� �������� ������

	}
	Cache(int Size)//������ ���������� � ��
	{
		this->Size = Size;
		SizeB = Size * 1024 * 1024;
		p = new unsigned char[SizeB];//������ � ������
		for (int i = 0; i < SizeB; i++)
		{
			p[i] = rand();
		}
	}
	Cache()
	{
		p = nullptr;
	}
	void Test(ofstream& fout, int rez, int num)
	{
		unsigned char A;
		auto start = chrono::system_clock::now();           //������ �����

		for (int i = 0; i < N; i++)
		{
			switch (rez) {
			case 1://������ ������

				for (int j = 0; j < SizeB; j++)
				{
					A = p[j];
				}
				break;
			case 2://�������� ������
				for (int j = SizeB - 1; j >= 0; j--)
				{
					A = p[j];
				}
				break;
			case 3://��������� ������

				break;
			}
		}
		auto end = chrono::system_clock::now();   //����� �����
		chrono::duration<double> time = end - start;
		cout << endl << time.count() << " sec";
		fout << endl << "travel order: ";
		switch (rez) {
		case 1://������ ������

			fout << "direction";
			break;
		case 2://�������� ������
			fout << "nodirection";
			break;
		case 3:
			fout << "random";
			break;
		}
		fout << endl << "Number: " << num;
		fout << endl << "buffer size: " << Size << "mb";
		fout << endl << "duration: " << time.count() << " sec";


	}
	~Cache()
	{
		if (p != nullptr)
		{
			delete[]p;
		}
	}


};

int Cache::N = 1000;//������������� ������������ ���� ������
int main()
{
	vector<Cache> V;
	Cache Ob1(1), Ob2(2), Ob3(4), Ob4(8), ob5(12);
	V.push_back(Ob1);
	V.push_back(Ob2);
	V.push_back(Ob3);
	V.push_back(Ob4);
	V.push_back(ob5);
	ofstream fout("out.txt");
	int i = 1;
	for (auto pos : V)
	{
		pos.Test(fout, 1, i);
		i++;
		pos.Test(fout, 2, i);
		i++;
	}
	fout.close();

	//Ob1.Test();

	system("pause");
	return 0;

}

// ������ ���������: CTRL+F5 ��� ���� "�������" > "������ ��� �������"
// ������� ���������: F5 ��� ���� "�������" > "��������� �������"

// ������ �� ������ ������ 
//   1. � ���� ������������ ������� ����� ��������� ����� � ��������� ���.
//   2. � ���� Team Explorer ����� ������������ � ������� ���������� ��������.
//   3. � ���� "�������� ������" ����� ������������� �������� ������ ������ � ������ ���������.
//   4. � ���� "������ ������" ����� ������������� ������.
//   5. ��������������� �������� ������ ���� "������" > "�������� ����� �������", ����� ������� ����� ����, ��� "������" > "�������� ������������ �������", ����� �������� � ������ ������������ ����� ����.
//   6. ����� ����� ������� ���� ������ �����, �������� ������ ���� "����" > "�������" > "������" � �������� SLN-����.
